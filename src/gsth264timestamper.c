/* GStreamer
 * Copyright (C) 2022 Seungha Yang <seungha@centricular.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

/**
 * SECTION:element-h264timestamper
 * @title: h264timestamper
 *
 * A timestamp correction element for H.264 stream.
 *
 * ## Example launch line
 * ```
 * gst-launch-1.0 filesrc location=video.mkv ! matroskademux ! h264parse ! h264timestamper ! mp4mux ! filesink location=output.mp4
 * ```
 *
 */

/* TODO:
 * Parse POC and correct PTS if it's is unknown
 * Reverse playback support
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gst/base/base.h>
#include <gst/codecparsers/gsth264parser.h>
#include "gsth264timestamper.h"
#include <string.h>

GST_DEBUG_CATEGORY (gst_h264_timestamper_debug);
#define GST_CAT_DEFAULT gst_h264_timestamper_debug

static GstStaticPadTemplate sinktemplate = GST_STATIC_PAD_TEMPLATE ("sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("video/x-h264, alignment=(string) au"));

static GstStaticPadTemplate srctemplate = GST_STATIC_PAD_TEMPLATE ("src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("video/x-h264, alignment=(string) au"));

typedef struct
{
  GstBuffer *buffer;
  GList *events;

  GstClockTime pts;
} GstH264Frame;

struct _GstH264Timestamper
{
  GstElement parent;
  GstPad *sinkpad;
  GstPad *srcpad;

  GRecMutex lock;

  GstSegment in_segment;

  GstH264NalParser *parser;
  gboolean packetized;
  guint nal_length_size;

  GList *current_frame_events;
  GstQueueArray *queue;
  GArray *timestamp_queue;

  gint fps_n;
  gint fps_d;

  guint max_bframes;
  guint max_dpb_frames;
  guint max_reorder_frames;
  gboolean interlaced;
  guint window_size;
  GstClockTime last_dts;
  GstClockTime dts_offset;
  GstClockTime time_adjustment;

  GstClockTime latency;
};

static void gst_h264_timestamper_finalize (GObject * object);
static GstFlowReturn gst_h264_timestamper_chain (GstPad * pad,
    GstObject * parent, GstBuffer * buffer);
static gboolean gst_h264_timestamper_sink_event (GstPad * pad,
    GstObject * parent, GstEvent * event);
static gboolean gst_h264_timestamper_src_query (GstPad * pad,
    GstObject * parent, GstQuery * query);
static GstStateChangeReturn
gst_h264_timestamper_change_state (GstElement * element,
    GstStateChange transition);
static void gst_h264_timestamper_clear_frame (GstH264Frame * frame);
static void gst_h264_timestamper_reset (GstH264Timestamper * self);
static void gst_h264_timestamper_process_nal (GstH264Timestamper * self,
    GstH264Frame * frame, GstH264NalUnit * nalu);
static void gst_h264_timestamper_drain (GstH264Timestamper * self);

#define gst_h264_timestamper_parent_class parent_class
G_DEFINE_TYPE (GstH264Timestamper, gst_h264_timestamper, GST_TYPE_ELEMENT);

/* API added in 1.21 */
#if !GST_CHECK_VERSION (1, 21, 0)
typedef struct _GstH264DecoderConfigRecord    GstH264DecoderConfigRecord;

struct _GstH264DecoderConfigRecord
{
  /**
   * GstH264DecoderConfigRecord.configuration_version:
   *
   * Indicates configurationVersion, must be 1
   */
  guint8 configuration_version;

  /**
   * GstH264DecoderConfigRecord.profile_indication:
   *
   * H.264 profile indication
   */
  guint8 profile_indication;

  /**
   * GstH264DecoderConfigRecord.profile_compatibility:
   *
   * H.264 profile compatibility
   */
  guint8 profile_compatibility;

  /**
   * GstH264DecoderConfigRecord.level_indication:
   *
   * H.264 level indiction
   */
  guint8 level_indication;

  /**
   * GstH264DecoderConfigRecord.length_size_minus_one:
   *
   * Indicates the length in bytes of the NAL unit length field
   */
  guint8 length_size_minus_one;

  /**
   * GstH264DecoderConfigRecord.sps
   *
   * Array of identified #GstH264NalUnit from sequenceParameterSetNALUnit.
   * This array may contain non-SPS nal units such as SEI message
   */
  GArray *sps;

  /**
   * GstH264DecoderConfigRecord.pps
   *
   * Array of identified #GstH264NalUnit from pictureParameterSetNALUnit.
   * This array may contain non-PPS nal units such as SEI message
   */
  GArray *pps;

  /**
   * GstH264DecoderConfigRecord.chroma_format_present
   *
   * %TRUE if chroma information is present. Otherwise below values
   * have no meaning
   */
  gboolean chroma_format_present;

  /**
   * GstH264DecoderConfigRecord.chroma_format
   *
   * chroma_format_idc defined in ISO/IEC 14496-10
   */
  guint8 chroma_format;

  /**
   * GstH264DecoderConfigRecord.bit_depth_luma_minus8
   *
   * Indicates bit depth of luma component
   */
  guint8 bit_depth_luma_minus8;

  /**
   * GstH264DecoderConfigRecord.bit_depth_chroma_minus8
   *
   * Indicates bit depth of chroma component
   */
  guint8 bit_depth_chroma_minus8;

  /**
   * GstH264DecoderConfigRecord.sps_ext
   *
   * Array of identified #GstH264NalUnit from sequenceParameterSetExtNALUnit.
   */
  GArray *sps_ext;

  /*< private >*/
  gpointer _gst_reserved[GST_PADDING];
};

static GstH264DecoderConfigRecord *
gst_h264_decoder_config_record_new (void)
{
  GstH264DecoderConfigRecord *config;

  config = g_new0 (GstH264DecoderConfigRecord, 1);
  config->sps = g_array_new (FALSE, FALSE, sizeof (GstH264NalUnit));
  config->pps = g_array_new (FALSE, FALSE, sizeof (GstH264NalUnit));
  config->sps_ext = g_array_new (FALSE, FALSE, sizeof (GstH264NalUnit));

  return config;
}

static void
gst_h264_decoder_config_record_free (GstH264DecoderConfigRecord * config)
{
  if (!config)
    return;

  if (config->sps)
    g_array_unref (config->sps);

  if (config->pps)
    g_array_unref (config->pps);

  if (config->sps_ext)
    g_array_unref (config->sps_ext);

  g_free (config);
}

static GstH264ParserResult
gst_h264_parser_parse_decoder_config_record (GstH264NalParser * nalparser,
    const guint8 * data, gsize size, GstH264DecoderConfigRecord ** config)
{
  GstH264DecoderConfigRecord *ret;
  GstBitReader br;
  GstH264ParserResult result = GST_H264_PARSER_OK;
  guint8 num_sps, num_pps, i;
  guint offset;

  g_return_val_if_fail (nalparser != NULL, GST_H264_PARSER_ERROR);
  g_return_val_if_fail (data != NULL, GST_H264_PARSER_ERROR);
  g_return_val_if_fail (config != NULL, GST_H264_PARSER_ERROR);

#define READ_CONFIG_UINT8(val, nbits) G_STMT_START { \
  if (!gst_bit_reader_get_bits_uint8 (&br, &val, nbits)) { \
    GST_WARNING ("Failed to read " G_STRINGIFY (val)); \
    result = GST_H264_PARSER_ERROR; \
    goto error; \
  } \
} G_STMT_END;

#define SKIP_CONFIG_BITS(nbits) G_STMT_START { \
  if (!gst_bit_reader_skip (&br, nbits)) { \
    GST_WARNING ("Failed to skip %d bits", nbits); \
    result = GST_H264_PARSER_ERROR; \
    goto error; \
  } \
} G_STMT_END;

  *config = NULL;

  if (size < 7) {
    GST_WARNING ("Too small size avcC");
    return GST_H264_PARSER_ERROR;
  }

  gst_bit_reader_init (&br, data, size);

  ret = gst_h264_decoder_config_record_new ();

  READ_CONFIG_UINT8 (ret->configuration_version, 8);
  /* Keep parsing, caller can decide whether this data needs to be discarded
   * or not */
  if (ret->configuration_version != 1) {
    GST_WARNING ("Wrong configurationVersion %d", ret->configuration_version);
    result = GST_H264_PARSER_ERROR;
    goto error;
  }

  READ_CONFIG_UINT8 (ret->profile_indication, 8);
  READ_CONFIG_UINT8 (ret->profile_compatibility, 8);
  READ_CONFIG_UINT8 (ret->level_indication, 8);
  /* reserved 6bits */
  SKIP_CONFIG_BITS (6);
  READ_CONFIG_UINT8 (ret->length_size_minus_one, 2);
  if (ret->length_size_minus_one == 2) {
    /* "length_size_minus_one + 1" should be 1, 2, or 4 */
    GST_WARNING ("Wrong nal-length-size");
    result = GST_H264_PARSER_ERROR;
    goto error;
  }

  /* reserved 3bits */
  SKIP_CONFIG_BITS (3);

  READ_CONFIG_UINT8 (num_sps, 5);
  offset = gst_bit_reader_get_pos (&br);

  g_assert (offset % 8 == 0);
  offset /= 8;
  for (i = 0; i < num_sps; i++) {
    GstH264NalUnit nalu;

    result = gst_h264_parser_identify_nalu_avc (nalparser,
        data, offset, size, 2, &nalu);
    if (result != GST_H264_PARSER_OK)
      goto error;

    g_array_append_val (ret->sps, nalu);
    offset = nalu.offset + nalu.size;
  }

  if (!gst_bit_reader_set_pos (&br, offset * 8)) {
    result = GST_H264_PARSER_ERROR;
    goto error;
  }

  READ_CONFIG_UINT8 (num_pps, 8);
  offset = gst_bit_reader_get_pos (&br);

  g_assert (offset % 8 == 0);
  offset /= 8;
  for (i = 0; i < num_pps; i++) {
    GstH264NalUnit nalu;

    result = gst_h264_parser_identify_nalu_avc (nalparser,
        data, offset, size, 2, &nalu);
    if (result != GST_H264_PARSER_OK)
      goto error;

    g_array_append_val (ret->pps, nalu);
    offset = nalu.offset + nalu.size;
  }

  /* Parse chroma format and SPS ext data. We will silently ignore any
   * error while parsing below data since it's not essential data for
   * decoding */
  if (ret->profile_indication == 100 || ret->profile_indication == 110 ||
      ret->profile_indication == 122 || ret->profile_indication == 144) {
    guint8 num_sps_ext;

    if (!gst_bit_reader_set_pos (&br, offset * 8))
      goto out;

    if (!gst_bit_reader_skip (&br, 6))
      goto out;

    if (!gst_bit_reader_get_bits_uint8 (&br, &ret->chroma_format, 2))
      goto out;

    if (!gst_bit_reader_skip (&br, 5))
      goto out;

    if (!gst_bit_reader_get_bits_uint8 (&br, &ret->bit_depth_luma_minus8, 3))
      goto out;

    if (!gst_bit_reader_skip (&br, 5))
      goto out;

    if (!gst_bit_reader_get_bits_uint8 (&br, &ret->bit_depth_chroma_minus8, 3))
      goto out;

    if (!gst_bit_reader_get_bits_uint8 (&br, &num_sps_ext, 8))
      goto out;

    offset = gst_bit_reader_get_pos (&br);

    g_assert (offset % 8 == 0);
    offset /= 8;
    for (i = 0; i < num_sps_ext; i++) {
      GstH264NalUnit nalu;

      result = gst_h264_parser_identify_nalu_avc (nalparser,
          data, offset, size, 2, &nalu);
      if (result != GST_H264_PARSER_OK)
        goto out;

      g_array_append_val (ret->sps_ext, nalu);
      offset = nalu.offset + nalu.size;
    }

    ret->chroma_format_present = TRUE;
  }

out:
  {
    *config = ret;
    return GST_H264_PARSER_OK;
  }
error:
  {
    gst_h264_decoder_config_record_free (ret);
    return result;
  }

#undef READ_CONFIG_UINT8
#undef SKIP_CONFIG_BITS
}
#endif

static void
gst_h264_timestamper_class_init (GstH264TimestamperClass * klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GstElementClass *element_class = GST_ELEMENT_CLASS (klass);

  object_class->finalize = gst_h264_timestamper_finalize;

  element_class->change_state =
      GST_DEBUG_FUNCPTR (gst_h264_timestamper_change_state);

  gst_element_class_add_static_pad_template (element_class, &sinktemplate);
  gst_element_class_add_static_pad_template (element_class, &srctemplate);

  gst_element_class_set_static_metadata (element_class, "H.264 timestamper",
      "Codec/Parser/Video", "Timestamp H.264 streams",
      "Seungha Yang <seungha@centricular.com>");

  GST_DEBUG_CATEGORY_INIT (gst_h264_timestamper_debug, "h264timestamper", 0,
      "h264timestamper");
}

static void
gst_h264_timestamper_init (GstH264Timestamper * self)
{
  self->sinkpad = gst_pad_new_from_static_template (&sinktemplate, "sink");
  gst_pad_set_chain_function (self->sinkpad,
      GST_DEBUG_FUNCPTR (gst_h264_timestamper_chain));
  gst_pad_set_event_function (self->sinkpad,
      GST_DEBUG_FUNCPTR (gst_h264_timestamper_sink_event));
  GST_PAD_SET_PROXY_SCHEDULING (self->sinkpad);
  gst_element_add_pad (GST_ELEMENT (self), self->sinkpad);

  self->srcpad = gst_pad_new_from_static_template (&srctemplate, "src");
  gst_pad_set_query_function (self->srcpad,
      GST_DEBUG_FUNCPTR (gst_h264_timestamper_src_query));
  GST_PAD_SET_PROXY_SCHEDULING (self->srcpad);

  gst_element_add_pad (GST_ELEMENT (self), self->srcpad);

  self->queue = gst_queue_array_new_for_struct (sizeof (GstH264Frame), 16);
  gst_queue_array_set_clear_func (self->queue,
      (GDestroyNotify) gst_h264_timestamper_clear_frame);
  self->timestamp_queue = g_array_sized_new (FALSE, FALSE,
      sizeof (GstClockTime), 16);

  g_rec_mutex_init (&self->lock);
}

static void
gst_h264_timestamper_finalize (GObject * object)
{
  GstH264Timestamper *self = GST_H264_TIMESTAMPER (object);

  gst_h264_timestamper_reset (self);
  gst_queue_array_free (self->queue);
  g_array_unref (self->timestamp_queue);
  g_rec_mutex_clear (&self->lock);

  G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
gst_h264_timestamper_set_caps (GstH264Timestamper * self, GstCaps * caps)
{
  GstStructure *s = gst_caps_get_structure (caps, 0);
  const gchar *str;
  gboolean found_format = FALSE;
  const GValue *codec_data_val;

  self->packetized = FALSE;
  self->nal_length_size = 4;
  str = gst_structure_get_string (s, "stream-format");
  if (g_strcmp0 (str, "avc") == 0 || g_strcmp0 (str, "avc3") == 0) {
    self->packetized = TRUE;
    found_format = TRUE;
  } else if (g_strcmp0 (str, "byte-stream") == 0) {
    found_format = TRUE;
  }

  self->fps_n = 0;
  self->fps_d = 1;

  gst_structure_get_fraction (s, "framerate", &self->fps_n, &self->fps_d);

  if (self->fps_n <= 0 && self->fps_d <= 0) {
    /* TODO: Try parsing framerate via picture timing SEI */
    GST_WARNING_OBJECT (self, "Unknown frame rate, assume 25/1");
    self->fps_n = 25;
    self->fps_d = 1;
  }

  codec_data_val = gst_structure_get_value (s, "codec_data");
  if (codec_data_val && GST_VALUE_HOLDS_BUFFER (codec_data_val)) {
    GstBuffer *codec_data = gst_value_get_buffer (codec_data_val);
    GstMapInfo map;
    GstH264NalUnit *nalu;
    GstH264ParserResult ret;
    GstH264DecoderConfigRecord *config = NULL;
    guint i;

    if (!gst_buffer_map (codec_data, &map, GST_MAP_READ)) {
      GST_ERROR_OBJECT (self, "Unable to map codec-data buffer");
      return;
    }

    ret = gst_h264_parser_parse_decoder_config_record (self->parser,
        map.data, map.size, &config);
    if (ret != GST_H264_PARSER_OK) {
      GST_WARNING_OBJECT (self, "Failed to parse codec-data");
      goto unmap;
    }

    self->nal_length_size = config->length_size_minus_one + 1;
    for (i = 0; i < config->sps->len; i++) {
      nalu = &g_array_index (config->sps, GstH264NalUnit, i);
      gst_h264_timestamper_process_nal (self, NULL, nalu);
    }

    for (i = 0; i < config->pps->len; i++) {
      nalu = &g_array_index (config->pps, GstH264NalUnit, i);
      gst_h264_timestamper_process_nal (self, NULL, nalu);
    }

    /* codec_data would mean packetized format */
    if (!found_format)
      self->packetized = TRUE;

  unmap:
    gst_buffer_unmap (codec_data, &map);
    g_clear_pointer (&config, gst_h264_decoder_config_record_free);
  }
}

static gboolean
gst_h264_timestamper_push_event (GstH264Timestamper * self, GstEvent * event)
{
  if (GST_EVENT_TYPE (event) == GST_EVENT_SEGMENT) {
    GstSegment segment;
    guint32 seqnum;

    gst_event_copy_segment (event, &segment);

    if (segment.format != GST_FORMAT_TIME) {
      GST_ELEMENT_ERROR (self, CORE, EVENT, (NULL),
          ("Non-time format segment"));
      gst_event_unref (event);
      return FALSE;
    }

    if (self->time_adjustment != GST_CLOCK_TIME_NONE) {
      segment.start += self->time_adjustment;
      if (GST_CLOCK_TIME_IS_VALID (segment.position))
        segment.position += self->time_adjustment;
      if (GST_CLOCK_TIME_IS_VALID (segment.stop))
        segment.stop += self->time_adjustment;
    }

    seqnum = gst_event_get_seqnum (event);

    gst_event_unref (event);
    event = gst_event_new_segment (&segment);
    gst_event_set_seqnum (event, seqnum);
  }

  return gst_pad_push_event (self->srcpad, event);
}

static void
gst_h264_timestamper_flush_events (GstH264Timestamper * self, GList ** events)
{
  GList *iter;

  for (iter = *events; iter; iter = g_list_next (iter)) {
    GstEvent *ev = GST_EVENT (iter->data);

    if (GST_EVENT_IS_STICKY (ev) && GST_EVENT_TYPE (ev) != GST_EVENT_EOS &&
        GST_EVENT_TYPE (ev) != GST_EVENT_SEGMENT) {
      gst_pad_store_sticky_event (self->srcpad, ev);
    }

    gst_event_unref (ev);
  }

  g_clear_pointer (events, g_list_free);
}

static void
gst_h264_timestamper_flush (GstH264Timestamper * self)
{
  while (gst_queue_array_get_length (self->queue) > 0) {
    GstH264Frame *frame =
        (GstH264Frame *) gst_queue_array_pop_head_struct (self->queue);

    gst_h264_timestamper_flush_events (self, &frame->events);
    gst_h264_timestamper_clear_frame (frame);
  }

  gst_h264_timestamper_flush_events (self, &self->current_frame_events);

  self->time_adjustment = GST_CLOCK_TIME_NONE;
  self->last_dts = GST_CLOCK_TIME_NONE;
  g_rec_mutex_lock (&self->lock);
  self->latency = GST_CLOCK_TIME_NONE;
  g_rec_mutex_unlock (&self->lock);
}

static gboolean
gst_h264_timestamper_sink_event (GstPad * pad, GstObject * parent,
    GstEvent * event)
{
  GstH264Timestamper *self = GST_H264_TIMESTAMPER (parent);

  switch (GST_EVENT_TYPE (event)) {
    case GST_EVENT_CAPS:{
      GstCaps *caps;

      gst_event_parse_caps (event, &caps);
      gst_h264_timestamper_set_caps (self, caps);
      break;
    }
    case GST_EVENT_SEGMENT:{
      GstSegment segment;

      gst_event_copy_segment (event, &segment);
      if (segment.format != GST_FORMAT_TIME) {
        GST_WARNING_OBJECT (self, "Not a time format segment");
        gst_event_unref (event);
        return FALSE;
      }

      if (segment.rate < 0) {
        GST_WARNING_OBJECT (self, "Negative rate is not supported");
        gst_event_unref (event);
        return FALSE;
      }

      /* Drain on segment update */
      if (memcmp (&self->in_segment, &segment, sizeof (GstSegment)))
        gst_h264_timestamper_drain (self);

      self->in_segment = segment;
      break;
    }
    case GST_EVENT_EOS:
      gst_h264_timestamper_drain (self);
      if (self->current_frame_events) {
        GList *iter;

        for (iter = self->current_frame_events; iter; iter = g_list_next (iter))
          gst_h264_timestamper_push_event (self, GST_EVENT (iter->data));

        g_clear_pointer (&self->current_frame_events, g_list_free);
      }
      break;
    case GST_EVENT_FLUSH_STOP:
      gst_h264_timestamper_flush (self);
      break;
    default:
      break;
  }

  if (!GST_EVENT_IS_SERIALIZED (event) ||
      GST_EVENT_TYPE (event) == GST_EVENT_EOS ||
      GST_EVENT_TYPE (event) == GST_EVENT_FLUSH_STOP) {
    return gst_pad_event_default (pad, parent, event);
  }

  /* Store event to serialize queued frames */
  self->current_frame_events = g_list_append (self->current_frame_events,
      event);

  return TRUE;
}

static void
gst_h264_timestamper_frame_init (GstH264Frame * frame)
{
  memset (frame, 0, sizeof (GstH264Frame));

  frame->pts = GST_CLOCK_TIME_NONE;
}

static void
gst_h264_timestamper_clear_frame (GstH264Frame * frame)
{
  if (!frame)
    return;

  gst_clear_buffer (&frame->buffer);
  if (frame->events) {
    g_list_free_full (frame->events, (GDestroyNotify) gst_event_unref);
    frame->events = NULL;
  }
}

typedef enum
{
  GST_H264_LEVEL_L1 = 10,
  GST_H264_LEVEL_L1B = 9,
  GST_H264_LEVEL_L1_1 = 11,
  GST_H264_LEVEL_L1_2 = 12,
  GST_H264_LEVEL_L1_3 = 13,
  GST_H264_LEVEL_L2_0 = 20,
  GST_H264_LEVEL_L2_1 = 21,
  GST_H264_LEVEL_L2_2 = 22,
  GST_H264_LEVEL_L3 = 30,
  GST_H264_LEVEL_L3_1 = 31,
  GST_H264_LEVEL_L3_2 = 32,
  GST_H264_LEVEL_L4 = 40,
  GST_H264_LEVEL_L4_1 = 41,
  GST_H264_LEVEL_L4_2 = 42,
  GST_H264_LEVEL_L5 = 50,
  GST_H264_LEVEL_L5_1 = 51,
  GST_H264_LEVEL_L5_2 = 52,
  GST_H264_LEVEL_L6 = 60,
  GST_H264_LEVEL_L6_1 = 61,
  GST_H264_LEVEL_L6_2 = 62,
} GstH264DecoderLevel;

typedef struct
{
  GstH264DecoderLevel level;

  guint32 max_mbps;
  guint32 max_fs;
  guint32 max_dpb_mbs;
  guint32 max_main_br;
} LevelLimits;

static const LevelLimits level_limits_map[] = {
  {GST_H264_LEVEL_L1, 1485, 99, 396, 64},
  {GST_H264_LEVEL_L1B, 1485, 99, 396, 128},
  {GST_H264_LEVEL_L1_1, 3000, 396, 900, 192},
  {GST_H264_LEVEL_L1_2, 6000, 396, 2376, 384},
  {GST_H264_LEVEL_L1_3, 11800, 396, 2376, 768},
  {GST_H264_LEVEL_L2_0, 11880, 396, 2376, 2000},
  {GST_H264_LEVEL_L2_1, 19800, 792, 4752, 4000},
  {GST_H264_LEVEL_L2_2, 20250, 1620, 8100, 4000},
  {GST_H264_LEVEL_L3, 40500, 1620, 8100, 10000},
  {GST_H264_LEVEL_L3_1, 108000, 3600, 18000, 14000},
  {GST_H264_LEVEL_L3_2, 216000, 5120, 20480, 20000},
  {GST_H264_LEVEL_L4, 245760, 8192, 32768, 20000},
  {GST_H264_LEVEL_L4_1, 245760, 8192, 32768, 50000},
  {GST_H264_LEVEL_L4_2, 522240, 8704, 34816, 50000},
  {GST_H264_LEVEL_L5, 589824, 22080, 110400, 135000},
  {GST_H264_LEVEL_L5_1, 983040, 36864, 184320, 240000},
  {GST_H264_LEVEL_L5_2, 2073600, 36864, 184320, 240000},
  {GST_H264_LEVEL_L6, 4177920, 139264, 696320, 240000},
  {GST_H264_LEVEL_L6_1, 8355840, 139264, 696320, 480000},
  {GST_H264_LEVEL_L6_2, 16711680, 139264, 696320, 800000}
};

static guint
h264_level_to_max_dpb_mbs (GstH264DecoderLevel level)
{
  gint i;
  for (i = 0; i < G_N_ELEMENTS (level_limits_map); i++) {
    if (level == level_limits_map[i].level)
      return level_limits_map[i].max_dpb_mbs;
  }

  return 0;
}

static void
gst_h264_timestamper_process_sps (GstH264Timestamper * self, GstH264SPS * sps)
{
  guint8 level;
  guint max_dpb_mbs;
  guint width_mb, height_mb;
  guint max_dpb_frames = 0;
  guint max_reorder_frames = 0;
  gboolean interlaced = FALSE;
  gboolean update_latency = FALSE;

  if (sps->frame_mbs_only_flag == 0) {
    GST_DEBUG_OBJECT (self, "frame_mbs_only_flag is not set");
    interlaced = TRUE;
  }

  /* Spec A.3.1 and A.3.2
   * For Baseline, Constrained Baseline and Main profile, the indicated level is
   * Level 1b if level_idc is equal to 11 and constraint_set3_flag is equal to 1
   */
  level = sps->level_idc;
  if (level == 11 && (sps->profile_idc == 66 || sps->profile_idc == 77) &&
      sps->constraint_set3_flag) {
    /* Level 1b */
    level = 9;
  }

  max_dpb_mbs = h264_level_to_max_dpb_mbs ((GstH264DecoderLevel) level);
  if (sps->vui_parameters_present_flag
      && sps->vui_parameters.bitstream_restriction_flag) {
    max_dpb_frames = MAX (1, sps->vui_parameters.max_dec_frame_buffering);
  } else if (max_dpb_mbs != 0) {
    width_mb = sps->width / 16;
    height_mb = sps->height / 16;

    max_dpb_frames = MIN (max_dpb_mbs / (width_mb * height_mb), 16);
  } else {
    GST_WARNING_OBJECT (self, "Unable to get MAX DPB MBs");
    max_dpb_frames = 16;
  }

  GST_DEBUG_OBJECT (self, "Max DPB size %d", max_dpb_frames);

  max_reorder_frames = max_dpb_frames;
  if (sps->vui_parameters_present_flag
      && sps->vui_parameters.bitstream_restriction_flag) {
    max_reorder_frames = sps->vui_parameters.num_reorder_frames;
    if (max_reorder_frames > max_dpb_frames) {
      GST_WARNING_OBJECT (self, "num_reorder_frames %d > dpb size %d",
          max_reorder_frames, self->max_dpb_frames);
      max_reorder_frames = self->max_dpb_frames;
    }
  } else {
    if (sps->profile_idc == 66 || sps->profile_idc == 83) {
      /* baseline, constrained baseline and scalable-baseline profiles
         only contain I/P frames. */
      max_reorder_frames = 0;
    } else if (sps->constraint_set3_flag) {
      /* constraint_set3_flag may mean the -intra only profile. */
      switch (sps->profile_idc) {
        case 44:
        case 86:
        case 100:
        case 110:
        case 122:
        case 244:
          max_reorder_frames = 0;
          break;
        default:
          break;
      }
    }
  }

  GST_DEBUG_OBJECT (self, "Max num reorder frames %d", max_reorder_frames);

  g_rec_mutex_lock (&self->lock);
  if (self->interlaced != interlaced ||
      self->max_dpb_frames != max_dpb_frames ||
      self->max_reorder_frames != max_reorder_frames ||
      !GST_CLOCK_TIME_IS_VALID (self->latency)) {
    GstClockTime latency = 0;
    GstClockTime dts_offset = 0;
    guint window_size = 0;

    self->interlaced = interlaced;
    self->max_dpb_frames = max_dpb_frames;
    self->max_reorder_frames = max_reorder_frames;

    /* No reordering, PTS == DTS */
    if (max_reorder_frames == 0) {
      window_size = 0;
      latency = 0;
    } else {
      dts_offset = gst_util_uint64_scale_int (max_reorder_frames * GST_SECOND,
          self->fps_d, self->fps_n);

      window_size = max_reorder_frames;

      /* XXX: AU aligned frame may contains single field or complete
       * field pair */
      if (interlaced)
        window_size *= 2;

      /* Add margin to be robust against PTS errors and in order for boundary
       * frames' PTS can be referenced */
      window_size += 2;
      latency = gst_util_uint64_scale_int (window_size * GST_SECOND,
          self->fps_d, self->fps_n);
    }

    self->window_size = window_size;
    self->dts_offset = dts_offset;

    if (latency != self->latency) {
      self->latency = latency;
      update_latency = TRUE;
    }
  }
  g_rec_mutex_unlock (&self->lock);

  if (update_latency) {
    gst_element_post_message (GST_ELEMENT_CAST (self),
        gst_message_new_latency (GST_OBJECT_CAST (self)));
  }
}

static void
gst_h264_timestamper_process_nal (GstH264Timestamper * self,
    GstH264Frame * frame, GstH264NalUnit * nalu)
{
  GstH264ParserResult ret;

  switch (nalu->type) {
    case GST_H264_NAL_SPS:{
      GstH264SPS sps;
      ret = gst_h264_parser_parse_sps (self->parser, nalu, &sps);
      if (ret != GST_H264_PARSER_OK) {
        GST_WARNING_OBJECT (self, "Failed to parse SPS");
        break;
      }

      gst_h264_timestamper_process_sps (self, &sps);
      gst_h264_sps_clear (&sps);
      break;
    }
      /* TODO: parse PPS/SLICE and correct PTS based on POC if needed */
    default:
      break;
  }
}

static GstFlowReturn
gst_h264_timestamper_output_frame (GstH264Timestamper * self,
    GstH264Frame * frame)
{
  GList *iter;
  GstFlowReturn ret;
  GstClockTime dts = GST_CLOCK_TIME_NONE;

  for (iter = frame->events; iter; iter = g_list_next (iter)) {
    GstEvent *event = GST_EVENT (iter->data);

    gst_h264_timestamper_push_event (self, event);
  }

  g_clear_pointer (&frame->events, g_list_free);

  if (GST_CLOCK_TIME_IS_VALID (frame->pts)) {
    g_assert (self->timestamp_queue->len > 0);
    dts = g_array_index (self->timestamp_queue, GstClockTime, 0);
    g_array_remove_index (self->timestamp_queue, 0);

    if (GST_CLOCK_TIME_IS_VALID (self->dts_offset))
      dts -= self->dts_offset;
  }

  if (GST_CLOCK_TIME_IS_VALID (dts)) {
    if (!GST_CLOCK_TIME_IS_VALID (self->last_dts))
      self->last_dts = dts;

    /* make sure DTS <= PTS */
    if (GST_CLOCK_TIME_IS_VALID (frame->pts)) {
      if (dts > frame->pts) {
        if (frame->pts >= self->last_dts)
          dts = frame->pts;
        else
          dts = GST_CLOCK_TIME_NONE;
      }

      if (GST_CLOCK_TIME_IS_VALID (dts))
        self->last_dts = dts;
    }
  }

  frame->buffer = gst_buffer_make_writable (frame->buffer);
  GST_BUFFER_PTS (frame->buffer) = frame->pts;
  GST_BUFFER_DTS (frame->buffer) = dts;

  GST_TRACE_OBJECT (self, "Output %" GST_PTR_FORMAT, frame->buffer);

  ret = gst_pad_push (self->srcpad, frame->buffer);
  frame->buffer = NULL;

  return ret;
}

static GstFlowReturn
gst_h264_timestamper_process_output_frame (GstH264Timestamper * self)
{
  guint len;
  GstH264Frame *frame;

  len = gst_queue_array_get_length (self->queue);
  if (len < self->window_size) {
    GST_TRACE_OBJECT (self, "Need more data, queued %d/%d", len,
        self->window_size);
    return GST_FLOW_OK;
  }

  frame = (GstH264Frame *) gst_queue_array_pop_head_struct (self->queue);

  return gst_h264_timestamper_output_frame (self, frame);
}

static void
gst_h264_timestamper_drain (GstH264Timestamper * self)
{
  while (gst_queue_array_get_length (self->queue) > 0) {
    GstH264Frame *frame =
        (GstH264Frame *) gst_queue_array_pop_head_struct (self->queue);
    gst_h264_timestamper_output_frame (self, frame);
  }

  self->time_adjustment = GST_CLOCK_TIME_NONE;
  self->last_dts = GST_CLOCK_TIME_NONE;
}

static gint
pts_compare_func (const GstClockTime * a, const GstClockTime * b)
{
  return (*a) - (*b);
}

static GstFlowReturn
gst_h264_timestamper_chain (GstPad * pad, GstObject * parent,
    GstBuffer * buffer)
{
  GstH264Timestamper *self = GST_H264_TIMESTAMPER (parent);
  GstClockTime pts, dts;
  /* The same hack as x264 for negative DTS */
  static const GstClockTime min_pts = GST_SECOND * 60 * 60 * 1000;
  GstH264Frame frame;
  GstMapInfo map;

  gst_h264_timestamper_frame_init (&frame);

  GST_TRACE_OBJECT (self, "Handle %" GST_PTR_FORMAT, buffer);

  pts = GST_BUFFER_PTS (buffer);
  dts = GST_BUFFER_DTS (buffer);

  if (!GST_CLOCK_TIME_IS_VALID (self->time_adjustment)) {
    GstClockTime start_time = GST_CLOCK_TIME_NONE;

    if (GST_CLOCK_TIME_IS_VALID (pts))
      start_time = MAX (pts, self->in_segment.start);
    else if (GST_CLOCK_TIME_IS_VALID (dts))
      start_time = MAX (dts, self->in_segment.start);
    else
      start_time = self->in_segment.start;

    if (start_time < min_pts)
      self->time_adjustment = min_pts - start_time;
  }

  if (GST_CLOCK_TIME_IS_VALID (self->time_adjustment)) {
    if (GST_CLOCK_TIME_IS_VALID (pts))
      pts += self->time_adjustment;
    if (GST_CLOCK_TIME_IS_VALID (dts))
      dts += self->time_adjustment;
  }

  frame.pts = pts;
  frame.buffer = buffer;
  frame.events = self->current_frame_events;
  self->current_frame_events = NULL;

  /* Ignore any error while parsing NAL */
  if (gst_buffer_map (buffer, &map, GST_MAP_READ)) {
    GstH264ParserResult ret;
    GstH264NalUnit nalu;

    if (self->packetized) {
      ret = gst_h264_parser_identify_nalu_avc (self->parser,
          map.data, 0, map.size, self->nal_length_size, &nalu);

      while (ret == GST_H264_PARSER_OK) {
        gst_h264_timestamper_process_nal (self, &frame, &nalu);

        ret = gst_h264_parser_identify_nalu_avc (self->parser,
            map.data, nalu.offset + nalu.size, map.size, self->nal_length_size,
            &nalu);
      }
    } else {
      ret = gst_h264_parser_identify_nalu (self->parser,
          map.data, 0, map.size, &nalu);

      if (ret == GST_H264_PARSER_NO_NAL_END)
        ret = GST_H264_PARSER_OK;

      while (ret == GST_H264_PARSER_OK) {
        gst_h264_timestamper_process_nal (self, &frame, &nalu);

        ret = gst_h264_parser_identify_nalu (self->parser,
            map.data, nalu.offset + nalu.size, map.size, &nalu);

        if (ret == GST_H264_PARSER_NO_NAL_END)
          ret = GST_H264_PARSER_OK;
      }
    }
    gst_buffer_unmap (buffer, &map);
  }

  gst_queue_array_push_tail_struct (self->queue, &frame);
  if (GST_CLOCK_TIME_IS_VALID (frame.pts)) {
    g_array_append_val (self->timestamp_queue, frame.pts);
    g_array_sort (self->timestamp_queue, (GCompareFunc) pts_compare_func);
  }

  return gst_h264_timestamper_process_output_frame (self);
}

static gboolean
gst_h264_timestamper_src_query (GstPad * pad, GstObject * parent,
    GstQuery * query)
{
  GstH264Timestamper *self = GST_H264_TIMESTAMPER (parent);

  switch (GST_QUERY_TYPE (query)) {
    case GST_QUERY_LATENCY:{
      gboolean ret;

      ret = gst_pad_peer_query (self->sinkpad, query);
      if (ret) {
        GstClockTime min, max;
        gboolean live;

        gst_query_parse_latency (query, &live, &min, &max);

        g_rec_mutex_lock (&self->lock);
        if (GST_CLOCK_TIME_IS_VALID (self->latency))
          min += self->latency;
        g_rec_mutex_unlock (&self->lock);

        gst_query_set_latency (query, live, min, max);
      }

      return ret;
    }
    default:
      break;
  }

  return gst_pad_query_default (pad, parent, query);
}

static void
gst_h264_timestamper_reset (GstH264Timestamper * self)
{
  g_clear_pointer (&self->parser, gst_h264_nal_parser_free);
  gst_queue_array_clear (self->queue);
  g_array_set_size (self->timestamp_queue, 0);
  self->fps_n = 0;
  self->fps_d = 1;
  self->dts_offset = 0;
  self->time_adjustment = GST_CLOCK_TIME_NONE;
  self->latency = GST_CLOCK_TIME_NONE;
  self->max_bframes = 0;
  self->max_dpb_frames = 0;
  self->max_reorder_frames = 0;
  self->interlaced = FALSE;
  self->window_size = 0;
  self->last_dts = GST_CLOCK_TIME_NONE;

  if (self->current_frame_events) {
    g_list_free_full (self->current_frame_events,
        (GDestroyNotify) gst_event_unref);
    self->current_frame_events = NULL;
  }
}

static gboolean
gst_h264_timestamper_start (GstH264Timestamper * self)
{
  gst_h264_timestamper_reset (self);

  self->parser = gst_h264_nal_parser_new ();

  return TRUE;
}

static GstStateChangeReturn
gst_h264_timestamper_change_state (GstElement * element,
    GstStateChange transition)
{
  GstH264Timestamper *self = GST_H264_TIMESTAMPER (element);
  GstStateChangeReturn ret;

  switch (transition) {
    case GST_STATE_CHANGE_READY_TO_PAUSED:
      gst_h264_timestamper_start (self);
      break;
    default:
      break;
  }

  ret = GST_ELEMENT_CLASS (parent_class)->change_state (element, transition);

  switch (transition) {
    case GST_STATE_CHANGE_PAUSED_TO_READY:
      gst_h264_timestamper_reset (self);
      break;
    default:
      break;
  }

  return ret;
}
