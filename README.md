# Windows build
## 1. Prerequisites Setup

- Install git and python 3.6+
- install `meson` and `ninja` via Python's `pip`
- Visual Studio 2017 or newer
- GStreamer 1.20.x development package

## 2. Build
```sh
# Enter Visual Studio Developer Command Prompt
C:\Program Files\Microsoft Visual Studio\2022\Community> powershell
PS C:\Program Files\Microsoft Visual Studio\2022\Community> cd C:\SomeWorkingDir\gst-h264-timestamper

# Add GStreamer bin directory to PATH environment
PS C:\SomeWorkingDir\gst-h264-timestamper> $env:PATH += ";C:\gstreamer\1.0\msvc_x86_64\bin"

# Configure and build projbect
PS C:\SomeWorkingDir\gst-h264-timestamper> meson builddir
PS C:\SomeWorkingDir\gst-h264-timestamper> ninja -C builddir
```